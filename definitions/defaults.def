
################################################################################
# SIMPLE SOFIN DEFINITION RULESET:
#
#  Every definition file name must be a lowercase name. An example:
#     Problem: Software name contains capital letters (f.i. "ImageMagick")
#     Solution: Use "ImageMagick" as value of DEF_NAME, and definition
#       file will be called "imagemagick.def".
#
################################################################################


        DEF_COMPLIANCE='^1.[1-2]+.[0-9]+$' # defines version of Sofin required for this defaults (egrep format)

unset   DEF_TYPE # Set it to "meta" for meta bundles
unset   DEF_DISABLE_ON # defines list of unames. For example DEF_DISABLE_ON="Linux Darwin" will disable definition for Linux and OSX hosts.
unset   DEF_EXPORTS # defines list of binaries to be exported to user env into PATH
unset   DEF_FULL_NAME
unset   DEF_SOURCE_PATH # defines a path to source file (http, git, or any other supported by curl)
unset   DEF_GIT_MODE # set to use git repository as code source
unset   DEF_GIT_CHECKOUT # default git branch name if in git mode (master) or fallback to DEF_VERSION
unset   DEF_NAME # this is software name, but also part of source dir name - used to determine build directory after unpack
unset   DEF_POSTFIX # a postfix added to DEF_NAME if defined. Used in Sofin to do legacy versions of software.
unset   DEF_VERSION # defines software version
unset   DEF_REQUIREMENTS # list of definitions that given package is dependant on. Order is significant. Note that this is flat list.
unset   DEF_COMPILER_ARGS # additional special arguments for compiler(s) and preprocessor
unset   DEF_LINKER_ARGS # additional direct arguments to linker
        DEF_CONFIGURE_METHOD="./configure" # default configure script name
unset   DEF_CONFIGURE_ARGS # additional arguments to configuration script of software
        DEF_MAKE_METHOD="make ${MAKE_OPTS}"
        DEF_TEST_ENV="test_harness" # default TEST_ENV passed to test
        DEF_TEST_METHOD=":" # define to enable software internal testing, by default it is: make check/ test
        DEF_SKIPPED_DEFINITION_TEST="neon lzo bash python27 python35 perl cmake curl php70 php56 php55 lz4"
        DEF_INSTALL_METHOD="make ${MAKE_OPTS} install"
unset   DEF_BUILD_DIR_POSTFIX # special value required for software which delivers software sources in custom non standard directory.
unset   DEF_AFTER_UNPACK_METHOD
unset   DEF_AFTER_CONFIGURE_METHOD
unset   DEF_AFTER_MAKE_METHOD
unset   DEF_AFTER_TEST_METHOD
unset   DEF_AFTER_INSTALL_METHOD
unset   DEF_AFTER_PATCH_METHOD
unset   DEF_AFTER_EXPORT_METHOD
unset   DEF_SHA # sha1 of archive source, used to check file checksum after archive download. May be skipped when in git mode
unset   DEF_USER_INFO # additional optional information from definition to user displayed after installation
unset   DEF_CONFLICTS_WITH # defines list of definitions which are in conflict with software.
unset   DEF_REQUIRE_ROOT_ACCESS # set, to disallow definition to be built as non root users.
unset   DEF_APPLE_BUNDLE # set, to enable .app bundling for default (first) export.
unset   DEF_ORIGIN # app origin location, to gather newer software version if available
unset   DEF_STANDALONE # TODO: it's currently only a marker for standalone bundles
        DEF_STRIP=ALL # set to "ALL" to strip bins and libs; set to "BINS", to strip binaries only; set to "LIBS", to strip libs only, "NO" to skip strip
        #DEF_NO_GOLDEN_LINKER NOTE: since Sofin 0.92.x, golden linker is a default system linker. Unfortunatelly a few configuration scripts crases with explicit linker params set. In result - we explicitly unset golden linker specific arguments on configuration stage. Though under the hood - golden linker will be used anyway and will work just fine..
unset   DEF_NO_GOLDEN_LINKER # any value disables golden linker flags for configuration stage for current definition
unset   DEF_NO_LLVM_LINKER
unset   DEF_LINKER_NO_DTAGS # any value will cause omition of "-Wl,-rpath=${PREFIX}/lib,--enable-new-dtags" ld param.
unset   DEF_NO_FAST_MATH # any value disables "-ffast-math" compiler option for current definition
unset   DEF_NO_CCACHE # any value disables ccache usage for current definition
unset   DEF_USE_ALT_COMPILER # if set to YES, Sofin will try gcc/g++ from local PREFIX to build definition(s)
        DEF_DEFAULT_USELESS="info doc docs share/*-doc share/gtk-doc share/emacs share/common-lisp share/bison share/doc share/docs share/info"
        DEF_DEFAULT_USEFUL="bin/*-config bin/*cfg" # TODO:
unset   DEF_USELESS # list of patterns of files to be removed after build process. example: "lib/perl5 lib/python2.7 bin/perl bin/python"
unset   DEF_USEFUL # list of patterns of files to be left intact. It doesn't apply on patterns present on DEF_USELESS list
        DEF_CLEAN_USELESS=YES # if YES, all NOT exported binaries will be removed from PREFIX/bin and PREFIX/sbin

unset   DEF_NO_TRAP_INT_OVERFLOW # if set, pass "-ftrapv" to compiler
unset   DEF_SYSTEM_SPECIFIC_LDFLAGS # usually not necessary to set this

        DEF_DEFAULT_USED_ARCHITECTURE="x86_64" # deduplicated value of mainly used hw architecture
        DEF_WORKSTATION_VERSION_FLAGS="-mmacosx-version-min=10.11" # default major workstation system version used
        DEF_EXPLICIT_STDCXX_FLAGS="-std=c++11 -stdlib=libc++"
        DEF_VISIBILITY_FLAGS="-fvisibility=default -fvisibility-inlines-hidden" # explicit symbol visibility

        case "${SYSTEM_NAME}" in
            FreeBSD)
                # NOTE: alternate: -std=gnu++11:
                DEF_SYSTEM_SPECIFIC_CFLAGS="-DHAVE_LIBRESSL -arch ${DEF_DEFAULT_USED_ARCHITECTURE} ${DEF_VISIBILITY_FLAGS}"
                DEF_SYSTEM_SPECIFIC_CXXFLAGS="-DHAVE_LIBRESSL -arch ${DEF_DEFAULT_USED_ARCHITECTURE} ${DEF_EXPLICIT_STDCXX_FLAGS} ${DEF_VISIBILITY_FLAGS}"
                ;;

            Darwin)
                DEF_SYSTEM_SPECIFIC_CFLAGS="-arch=${DEF_DEFAULT_USED_ARCHITECTURE} ${DEF_WORKSTATION_VERSION_FLAGS} ${DEF_VISIBILITY_FLAGS}"
                DEF_SYSTEM_SPECIFIC_CXXFLAGS="-arch=${DEF_DEFAULT_USED_ARCHITECTURE} ${DEF_WORKSTATION_VERSION_FLAGS} ${DEF_EXPLICIT_STDCXX_FLAGS} ${DEF_VISIBILITY_FLAGS}"
                DEF_DEFAULT_USELESS="${DEF_DEFAULT_USELESS} share/acloc* share/automa* share/libtool share/bison share/readline" # include
                ;;

            Minix)
                DEF_SYSTEM_SPECIFIC_CFLAGS="-arch=i686 -I/usr/pkg/include ${DEF_VISIBILITY_FLAGS}"
                DEF_SYSTEM_SPECIFIC_CXXFLAGS="-arch=i686 ${DEF_EXPLICIT_STDCXX_FLAGS} ${DEF_VISIBILITY_FLAGS}"
                DEF_SYSTEM_SPECIFIC_LDFLAGS="-L/usr/pkg/lib"
                ;;

            Linux)
                DEF_SYSTEM_SPECIFIC_CFLAGS="-mno-avx ${DEF_VISIBILITY_FLAGS}"
                DEF_SYSTEM_SPECIFIC_CXXFLAGS="-mno-avx ${DEF_EXPLICIT_STDCXX_FLAGS} ${DEF_VISIBILITY_FLAGS}"
                DEF_DEFAULT_USELESS="${DEF_DEFAULT_USELESS} share/acloc* share/automa*" # include
                ;;
        esac


# # TODO: ideas:

# DEF_ENV=""

# DEF_REQUIREMENTS=""
# DEF_REQUIREMENTS_IMPORT="perl cmake" # => read perl.def, and include whole DEF_REQUIREMENTS value at once from each definition
