DEF_FULL_NAME="MariaDB 10.1"
DEF_STANDALONE=YES
DEF_SHA="345a108fd92474abc760ca803c121f76c8abef3c"
DEF_NAME="mariadb"
DEF_VERSION="10.1.18"
DEF_SOURCE_PATH="${MAIN_SOURCE_REPOSITORY}${DEF_NAME}-${DEF_VERSION}.tar.gz"
DEF_REQUIREMENTS="pkgconf make db libnet libedit lua51 pcre ccache zlib bzip2 xz lzma lzo lzop lz4 m4 perl yasm autoconf texinfo automake bison libtool cmake msgpack kytea libsodium zeromq4 expat libressl python27 libffi gettext glib libevent flex boost-minimal libxml2 mariadb-client" # ncurses
DEF_CONFIGURE_METHOD="cmake"
DEF_CONFIGURE_ARGS="-DINSTALL_SCRIPTDIR=bin -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci -DENABLED_LOCAL_INFILE=off -DWITH_LIBWRAP=1 -DPLUGIN_AUTH_GSSAPI_CLIENT=NO -DPLUGIN_TOKUDB=NO -DDISABLE_LIBMYSQLCLIENT_SYMBOL_VERSIONING=TRUE"
DEF_EXPORTS="mysql mysqld_multi mysqld mysqld_safe mysql_config mysqladmin mysqlimport mysqlbinlog mysqldump mysqlaccess mysqlcheck mysqlhotcopy mysql_upgrade mysql_install_db msql2mysql myisamchk myisamlog myisampack mysqltest mysqlslap mysqldumpslow mysqlbug mysql_zap mysql_waitpid mysql_tzinfo_to_sql mysql_setpermission mysql_secure_installation mysql_plugin mysql_fix_extensions mysql_find_rows mysql_convert_table_format mysql_client_test my_print_defaults myisam_ftdump innochecksum aria_chk aria_dump_log aria_ftdump aria_pack aria_read_log mytop perror mysqlshow mysql-proxy"
DEF_USELESS="share/bison share/gdb share/libtool doc sql-bench support-files data share/cmake* man lib/*perl* include/python* lib/python* doc docs sql-bench share/cmake* share/gtk* share/glib* share/man INSTALL-BINARY README COPYING share/gettext share/*.sql share/lua COPYING.LESSER COPYING.thirdparty CREDITS EXCEPTIONS-CLIENT README-wsrep scripts libexec lib/lua mysql-test include/boost"
DEF_USEFUL="bin/my*" # always be prepared for more ;)
DEF_CONFLICTS_WITH="Percona Mysql"

case "${SYSTEM_NAME}" in
    Linux)
        DEF_AFTER_INSTALL_METHOD="after_build"
        after_build () {
            mysql_proxy_name="mysql-proxy"
            mysql_proxy_version="0.8.5"
            mysql_proxy_archive="${mysql_proxy_name}-${mysql_proxy_version}.tar.gz"
            ${FETCH_BIN} ${MAIN_SOURCE_REPOSITORY}${mysql_proxy_archive} ${FETCH_OPTS}
            ${TAR_BIN} xf ${mysql_proxy_archive}
            cd ${mysql_proxy_name}-${mysql_proxy_version}
            PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig:/usr/lib/pkgconfig PATH=${PREFIX}/bin:/bin:/usr/bin ./configure --prefix=${PREFIX}
            make install
        }
        ;;

    FreeBSD)
        DEF_AFTER_PATCH_METHOD="sed_stuff"
        sed_stuff () {
            ${SED_BIN} -i '' -e 's|%%LOCALBASE%%|${LOCALBASE}|g' scripts/mysql_config.sh
            ${SED_BIN} -i '' -e 's|%%PREFIX%%|${PREFIX}|g' mysys/my_default.c
            ${SED_BIN} -i '' -e 's/*.1/${MAN1}/' man/CMakeLists.txt
        }
        DEF_CONFIGURE_ARGS="${DEF_CONFIGURE_ARGS} -DHAVE_STDCXX11=on"
        DEF_MAKE_METHOD="LD_LIBRARY_PATH=${PREFIX}/lib:/lib:/usr/lib make -s ${MAKE_OPTS}"
        DEF_INSTALL_METHOD="LD_LIBRARY_PATH=${PREFIX}/lib:/lib:/usr/lib make -s install ${MAKE_OPTS}"
        ;;
esac
