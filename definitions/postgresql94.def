DEF_FULL_NAME="PostgreSQL 9"
DEF_STANDALONE=YES
DEF_SHA="a8e6130576ff7b8876e71497f18cdfeb89bb6fc0"
DEF_NAME="postgresql"
DEF_POSTFIX="94"
DEF_VERSION="9.4.9"
DEF_SOURCE_PATH="${MAIN_SOURCE_REPOSITORY}${DEF_NAME}-${DEF_VERSION}.tar.bz2"
DEF_CONFIGURE_ARGS="--without-ldap --with-zlib --disable-debug --disable-nls --without-perl --without-python --without-libxml --with-openssl --disable-thread-safety --with-includes=${PREFIX}/include --with-libraries=${PREFIX}/lib"
DEF_REQUIREMENTS="pkgconf make m4 libtool libedit zlib libressl"
DEF_AFTER_INSTALL_METHOD="build_addons_and_extensions"
DEF_EXPORTS="postgres pg_ctl initdb pg_dump pg_dumpall pg_config psql createdb createuser dropdb pg_controldata pg_restore pg_resetxlog pg_archivecleanup pg_basebackup pg_isready pg_receivexlog pg_recvlogical pg_resetxlog postmaster reindexdb vacuumdb createlang droplang dropuser ecpg clusterdb"
DEF_ORIGIN="http://www.postgresql.org"
DEF_CONFLICTS_WITH="Postgresql"
DEF_STRIP=BIN
DEF_NO_FAST_MATH=YES
DEF_NO_LLVM_LINKER=YES

case "${SYSTEM_NAME}" in
    Linux)
        DEF_NO_GOLDEN_LINKER=YES
        ;;

    Darwin)
        # Do not strip binaries on OS X, due to:
        # PG::UndefinedFile: ERROR:  could not load library "/Software/Postgresql92/lib/postgresql/hstore.so": dlopen(/Software/Postgresql92/lib/postgresql/hstore.so, 10): Symbol not found: _CurrentMemoryContext
        DEF_STRIP=NO
        DEF_CONFIGURE_ARGS="${DEF_CONFIGURE_ARGS} --enable-dtrace"
        # NOTE: postgresql has to be built BEFORE unixodbc (id present on DEF_REQUIREMENTS list)
        ;;

    FreeBSD)
        # ...
        # <RhodiumToad> anyway, the main issue I know of is that freebsd's threading libs mess up the concept of RLIMIT_STACK for the initial thread of the process that breaks pg's stack limit checking, meaning that you can crash the backend in situations that ought to just produce an error message
        # <RhodiumToad> --enable-thread-safety only affects libpq, yes
        # <RhodiumToad> the problem is that dblink and postgres_fdw end up dynamically linking libpq into the backend
        DEF_CONFIGURE_ARGS="${DEF_CONFIGURE_ARGS} --enable-dtrace"
        DEF_LINKER_ARGS="-lelf"
        ;;

esac


build_addons_and_extensions () {
    base_dir="${BUILD_DIR}/${DEF_NAME}-${DEF_VERSION}/contrib"
    cd ${base_dir}/pg_archivecleanup && make install
    cd ${base_dir}/hstore && make install
    cd ${base_dir}/pg_trgm && make install
}
