DEF_FULL_NAME="Rustc - Rust-lang compiler"
DEF_SHA="5f5458164b0cb62ea1fbd4d4bf5a5309a4d6871e"
DEF_NAME="rustc"
DEF_VERSION="1.12.1"
DEF_SOURCE_PATH="${MAIN_SOURCE_REPOSITORY}${DEF_NAME}-${DEF_VERSION}-src.tar.gz"
DEF_REQUIREMENTS="pkgconf make m4 ccache libedit zlib bzip2 yaml lzma xz pcre gettext perl texinfo libidn gmp expat gdbm db sqlite re2c python27 ninja cmake unixodbc libressl libssh2 libevent spdylay nghttp2 curl llnextgen llvm git bison libunwind"
DEF_CONFIGURE_ARGS="--release-channel=stable --enable-clang --enable-ninja --enable-orbit --enable-jemalloc --default-linker=/usr/bin/clang --default-ar=${PREFIX}/bin/llvm-ar --enable-ccache --llvm-root=${PREFIX} --datadir=${PREFIX}/share --infodir=${PREFIX}/share/info --mandir=${PREFIX}/share/man --libdir=${PREFIX}/lib --disable-valgrind --disable-valgrind-rpass --disable-docs --enable-optimize --enable-optimize-cxx --enable-optimize-llvm --enable-dist-host-only --disable-optimize-tests"
unset DEFAULT_RUSTFLAGS
DEF_MAKE_METHOD="makeit"
makeit () {
    run "RUSTFLAGS=\"${DEFAULT_RUSTFLAGS}\" VERBOSE=0 ${PREFIX}/bin/make"
}
DEF_INSTALL_METHOD="installit"
installit () {
    run "RUSTFLAGS=\"${DEFAULT_RUSTFLAGS}\" CC=cc CPP=cpp CXX=c++ NO_REBUILD=1 VERBOSE=0 make install"
}
DEF_USEFUL="bin/rust* bin/*conf* bin/*.*sh bin/*cfg bin/*make bin/gettext* bin/libtool* bin/llvm* bin/*cache bin/git* bin/ninja bin/make bin/*cmake"
DEF_EXPORTS="rustc rustdoc rust-lldb rust-gdb"
DEF_USELESS="docs doc man include/python* lib/python* share/au* share/bison share/ac* share/info share/*doc include/perl* lib/*perl* share/common-lisp share/emacs share/examples share/git-core share/texinfo share/libxslt-plugins etc Services"
DEF_CONFLICTS_WITH="Rustc"
DEF_LINKER_ARGS="-L${PREFIX}/lib -L/usr/lib -lc++"
DEF_NO_CCACHE=YES # it's used internally anyway, but controlled by Rust build system

DEF_AFTER_PATCH_METHOD="sed_fixes"
sed_fixes () {
    # ${SED_BIN} -i '' -e "s|-g||g" mk/cfg/x86_64-unknown-freebsd.mk
    ${SED_BIN} -i '' -e "s|stdc++|c++|g" configure
    ${SED_BIN} -i '' -e "s|/usr/local|${PREFIX}|g" mk/cfg/x86_64-unknown-freebsd.mk src/rt/hoedown/Makefile src/jemalloc/configure src/jemalloc/configure.ac configure src/rust-installer/install-template.sh
    ${SED_BIN} -i '' -e "s|%%LOCALBASE%%|${PREFIX}|g" mk/main.mk
    return 0
}

if [ "Darwin" = "${SYSTEM_NAME}" ]; then
    DEF_CONFIGURE_ARGS="${DEF_CONFIGURE_ARGS} --disable-rpath --enable-rustbuild"
else # for non Darwin, use python from PREFIX:
    DEF_CONFIGURE_ARGS="${DEF_CONFIGURE_ARGS} --python=${PREFIX}/bin/python --enable-rpath --disable-rustbuild"
fi
